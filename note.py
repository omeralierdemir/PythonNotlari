t = [1, 2, 3, 1, 2, 5, 6, 7, 8]

s = []
for i in t:
       if i not in s:
          s.append(i)

print(s)  # bu kod listedeki duplicate elemanları temizlemektedir.


# set() fonksiyonu listenin tüm elemanlarını ayrıştırarak başka bir listeye veya 
# değişkene atmamızı sağlamaktadır. Bu işlem sırasında duplicate elemanları tek
# bir değişken olarak almakta. Kümelerdeki mantık (kümelerin gösterimi)

